## VOLUNTARIAT

Proiect national de voluntariat

beta.voluntariat.eu

Acest site trebuie sa corespunda mai multor criterii  
 - sa poata inregistra proiecte   
 - sa poata inregistra campanii (mai multe proiecte la un loc)  
 - sa poata inregistra voluntari  


Exemple utilitate aplicatie (cazuri)  
 - Plantare arbori zone defavorizate  
 - Campanie de sensibilizare donare sange  
 - Construirea de case pentru familii nevoiase  
 - Ecologizare zone insalubre (albii rauri, etc..)  
 - Campanii de strangere de fonduri   
 

Detalii tehnice  
 - tema folosita // METRONIC  
 - framework folosit // LARAVEL   
 - versiune PHP / 7  
 - baza de date / PHPMYADMIN / SQL  
 
 
Atentie!  
Pana ce va fi lansat, proiectul va fi actualizat pe http://beta.voluntariat.eu si va putea fi testat de orice parte interesata.  
Nu ne asumam raspunderea pentru pierderea datelor, confidentialitate, conflicte, iar subdomeniul beta este si trebuie considerat a fi de testare si doar de testare.  



Contact proiect
Dan Mihai Mariuta  
0745399958  

