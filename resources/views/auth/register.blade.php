@extends('layouts/dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
						<div class="form-group row {{ $errors->has('first_name') ? ' has-error' : '' }}"> 
							<label for="name" class="col-md-4 col-form-label text-md-right">Nume</label> 
							<div class="col-md-6"> @if(!empty($first_name)) <input id="name" type="text" class="form-control" name="first_name" value="{{$first_name}}" required autofocus> @else <input id="name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus> @endif @if ($errors->has('first_name')) 
								<span class="help-block"> 
									<strong> {{  $errors->first('first_name') }}</strong> 
								</span> @endif 
							</div> 
						</div> 
						<div class="form-group row {{ $errors->has('last_name') ? ' has-error' : '' }}"> 
							<label for="last_name" class="col-md-4 col-form-label text-md-right">Prenume</label> 
							<div class="col-md-6"> @if(!empty($last_name)) <input id="last_name" type="text" class="form-control" name="last_name" value="{{$last_name}}" required autofocus> @else <input id="name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus> @endif @if ($errors->has('last_name')) 
								<span class="help-block"> 
									<strong> {{  $errors->first('last_name') }}</strong> 
								</span> @endif 
							</div> 
						</div> 
						<div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}"> 
							<label for="email" class="col-md-4 col-form-label text-md-right">Email</label> 
							<div class="col-md-6"> @if(!empty($email)) 
								<input id="email" type="email" class="form-control" name="email" value="{{$email}}" required> @else <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required> @endif @if ($errors->has('email')) 
								<span class="help-block"> 
									<strong> {{ $errors->first('email') }} </strong> 
								</span> @endif 
							</div> 
						</div>
						
						
						

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Parola</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmati parola</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
