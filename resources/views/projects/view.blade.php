@extends('layouts/dashboard')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

	<div class="m-content">
		<div class="row">
			<div class="m-portlet m-portlet--full-height col-12">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Proiect de ecologizare a lacului Bicaz
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link active" data-toggle="tab" href="#descriere" role="tab">
									Descriere
								</a>
							</li>
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link" data-toggle="tab" href="#rapoarte" role="tab">
									Rapoarte
								</a>
							</li>
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link" data-toggle="tab" href="#conditii" role="tab">
									Conditii
								</a>
							</li>
							
							<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
								<a href="#" class=" nav-link m-tabs__link nav-link m-tabs__link m-dropdown__toggle dropdown-toggle btn--sm btn-secondary m-btn m-btn--label-danger">
									<i class='m-nav__link-icon	flaticon-danger'></i>Raporteaza
								</a>
								<div class="m-dropdown__wrapper" style="z-index: 101;">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 50.5px;"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav">
													<li class="m-nav__section m-nav__section--first">
														<span class="m-nav__section-text">
															Quick Actions
														</span>
													</li>
													<li class="m-nav__item">
														<a href="" class="m-nav__link">
															<i class="m-nav__link-icon flaticon-share"></i>
															<span class="m-nav__link-text">
																Activity
															</span>
														</a>
													</li>
													<li class="m-nav__item">
														<a href="" class="m-nav__link">
															<i class="m-nav__link-icon flaticon-chat-1"></i>
															<span class="m-nav__link-text">
																Messages
															</span>
														</a>
													</li>
													<li class="m-nav__item">
														<a href="" class="m-nav__link">
															<i class="m-nav__link-icon flaticon-info"></i>
															<span class="m-nav__link-text">
																FAQ
															</span>
														</a>
													</li>
													<li class="m-nav__item">
														<a href="" class="m-nav__link">
															<i class="m-nav__link-icon flaticon-lifebuoy"></i>
															<span class="m-nav__link-text">
																Support
															</span>
														</a>
													</li>
													<li class="m-nav__separator m-nav__separator--fit"></li>
													<li class="m-nav__item">
														<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
															Cancel
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="tab-content">
						<div class="tab-pane active" id="descriere">
							<div class='row'>
							<div class='col-md-4'>
								<img src='https://peterlengyel.files.wordpress.com/2014/10/dsc_5689.jpg' style='width:100%;'/>
							
							
							
							</div>
							<div class='col-md-8'>
								
								<div class="m-widget12">
									<div class="m-widget12__item">
										<span class="m-widget12__text1">
											<span>
												Leahu Mihai Alexandru
											</span>
											<br>
											Autor proiect
										</span>
										<span class="m-widget12__text2">
											<span>
												In desfasurare
											</span>
											<br>
											Status proiect
										</span>
									</div>
									<div class="m-widget12__item">
										<span class="m-widget12__text1">
											<span>
												19 - 25 august 2018
											</span>
											<br>
											Perioada desfasurare
										</span>
										<span class="m-widget12__text2">
											<span>
												49
											</span>
											<br>
											Voluntari inscrisi
										</span>
									</div>
									<div class="m-widget12__item">
										<span class="m-widget12__text1">
											<span>
												790 €
											</span>
											<br>
											Suma donata
										</span>
										<span class="m-widget12__text2">
											<span>
												120
											</span>
											<br>
											Maxim voluntari acceptati
										</span>
									</div>
									<div class="m-widget12__item">
										<span class="m-widget12__text1">
											Avarage Product Price
											<br>
											<span>
												$60,70
											</span>
										</span>
										<div class="m-widget12__text2">
											<div class="m-widget12__desc">
												Satisfication Rate
											</div>
											<br>
											<div class="m-widget12__progress">
												<div class="m-widget12__progress-sm progress m-progress--sm">
													<div class="m-widget12__progress-bar progress-bar bg-brand" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="m-widget12__stats">
													63%
												</span>
											</div>
										</div>
									</div>
								</div>
							
							
							</div>
							
							
							</div>
							
							
							
							
							
							
							
						</div>
						<div class="tab-pane" id="rapoarte">
							rapoarte
						</div>
						<div class="tab-pane" id="conditii">
							Conditii
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection