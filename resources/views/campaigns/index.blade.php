@extends('layouts/dashboard')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
 		<div class="mr-auto">
 			<h3 class="m-subheader__title m-subheader__title--separator">Campanii</h3>			
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
						   <i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="{{asset('/panou')}}" class="m-nav__link">
							<span class="m-nav__link-text">Panou Principal</span>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">Campanii</span>
						</a>
					</li>	
				</ul>
		</div>
		<div>
		</div>
	</div>
</div>
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				
				
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Ecologizare raul Bistrita - Piatra Neamt
								</h3>
								<h2 class="m-portlet__head-label m-portlet__head-label--danger">
									<span>Ecologizare</span>
								</h2>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cloud-upload"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-cog"></i>
									</a>
								</li>
								<li class="m-portlet__nav-item">
									<a href="" class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-share-alt-square"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						Ecologizarea raului Bistrita in dreptul orasului Piatra Neamt. Campanie organizata de catre un grup de ecologisti.
					<hr />
					<button type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">Vreau sa ma inscriu</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-default m-btn m-btn--custom">Mai multe..</button>
					</div>
				</div>
				
				
				
				
			</div>
		</div>
	</div>
</div>

@endsection