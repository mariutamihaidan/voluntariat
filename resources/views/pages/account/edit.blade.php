@extends('layouts/dashboard')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
 		<div class="mr-auto">
 			<h3 class="m-subheader__title m-subheader__title--separator">Contul Meu</h3>			
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
						   <i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="{{asset('/panou')}}" class="m-nav__link">
							<span class="m-nav__link-text">Panou Principal</span>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">Cont</span>
						</a>
					</li>	
				</ul>
		</div>
		<div>
		</div>
	</div>
</div>
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="row">
			<div class="col-xl-3 col-lg-4">
				<div class="m-portlet m-portlet--full-height  ">
					<div class="m-portlet__body">
						<div class="m-card-profile">
							<div class="m-card-profile__title m--hide">
								Your Profile
							</div>
							<div class="m-card-profile__pic">
								<div class="m-card-profile__pic-wrapper">
									<img src="assets/app/media/img/users/user4.jpg" alt=""/>
								</div>
							</div>
							<div class="m-card-profile__details">
								<span class="m-card-profile__name">
									Mark Andre
								</span>
								<a href="" class="m-card-profile__email m-link">
									mark.andre@gmail.com
								</a>
							</div>
						</div>
						<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
							<li class="m-nav__separator m-nav__separator--fit"></li>
							<li class="m-nav__section m--hide">
								<span class="m-nav__section-text">
									Section
								</span>
							</li>
							<li class="m-nav__item">
								<a href="header/profile&amp;demo=default.html" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-profile-1"></i>
									<span class="m-nav__link-title">
										<span class="m-nav__link-wrap">
											<span class="m-nav__link-text">
												My Profile
											</span>
											<span class="m-nav__link-badge">
												<span class="m-badge m-badge--success">
													2
												</span>
											</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="header/profile&amp;demo=default.html" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-share"></i>
									<span class="m-nav__link-text">
										Activity
									</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="header/profile&amp;demo=default.html" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-chat-1"></i>
									<span class="m-nav__link-text">
										Messages
									</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="header/profile&amp;demo=default.html" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-graphic-2"></i>
									<span class="m-nav__link-text">
										Sales
									</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="header/profile&amp;demo=default.html" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-time-3"></i>
									<span class="m-nav__link-text">
										Events
									</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="header/profile&amp;demo=default.html" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-lifebuoy"></i>
									<span class="m-nav__link-text">
										Support
									</span>
								</a>
							</li>
						</ul>
						<div class="m-portlet__body-separator"></div>
				
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-lg-8">
				<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
					<div class="m-portlet__head">
						<div class="m-portlet__head-tools">
							<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
										<i class="flaticon-share m--hide"></i>
										Editare cont
									</a>
								</li>
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link" data-toggle="tab" href="#retele_sociale" role="tab">
										Retele sociale
									</a>
								</li>
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link" data-toggle="tab" href="#confidentialitate" role="tab">
										Confidentialitate
									</a>
								</li>
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link" data-toggle="tab" href="#newsletter" role="tab">
										Newsletter
									</a>
								</li>
								
							</ul>
						</div>
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="m_user_profile_tab_1">
							<form action="{{asset('utilizatori/' . $user->id)}}" method='POST' class="m-form m-form--fit m-form--label-align-right">
								@csrf
								{{Form::hidden('_method','PUT')}}
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10 m--hide">
										<div class="alert m-alert m-alert--default" role="alert">
											The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">
												Datele mele
											</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Nume
										</label>
										<div class="col-7">
											<input class="form-control m-input" type="text" name='first_name' value="{{$user->first_name}}">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Prenume
										</label>
										<div class="col-7">
											<input class="form-control m-input" type="text" name='last_name' value="{{$user->last_name}}">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Email
										</label>
										<div class="col-7">
											<input class="form-control m-input" name='email' type="text" value="{{$user->email}}">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Judet
										</label>
										<div class="col-3">
											{{Form::select('county_id',
											$counties,
											$user->county_id,
											['class' => 'form-control m-input',
											'onchange' => 'countyChanged()'])
											}}
										</div>
										<div class="col-4">
											<select class="form-control m-input">
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2"></div>
											<div class="col-7">
												<button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
													Salvati modificarile
												</button>
												&nbsp;&nbsp;
												<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Anulati
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="tab-pane " id="retele_sociale">
							<form class="m-form m-form--fit m-form--label-align-right">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10 m--hide">
										<div class="alert m-alert m-alert--default" role="alert">
											The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">
												Retele sociale
											</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Linkedin
										</label>
										<div class="col-7">
											<input class="form-control m-input" type="text" value="www.linkedin.com/Mark.Andre">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Facebook
										</label>
										<div class="col-7">
											<input class="form-control m-input" type="text" value="www.facebook.com/Mark.Andre">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Twitter
										</label>
										<div class="col-7">
											<input class="form-control m-input" type="text" value="www.twitter.com/Mark.Andre">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Instagram
										</label>
										<div class="col-7">
											<input class="form-control m-input" type="text" value="www.instagram.com/Mark.Andre">
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2"></div>
											<div class="col-7">
												<button type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">
													Salvati modificarile
												</button>
												&nbsp;&nbsp;
												<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Anulati
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						
						</div>
						<div class="tab-pane " id="confidentialitate">
							<form class="m-form m-form--fit m-form--label-align-right">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10 m--hide">
										<div class="alert m-alert m-alert--default" role="alert">
											The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">
												Date personale
											</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10">
											<div class="form-group m-form__group row">
												<div class='col-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Ascundere email
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Ascundere email
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label> 
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Ascundere email
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Ascundere email
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-md-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Ascundere email
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-md-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Ascundere email
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						
						
						</div>
						<div class="tab-pane " id="newsletter">
							<form class="m-form m-form--fit m-form--label-align-right">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10 m--hide">
										<div class="alert m-alert m-alert--default" role="alert">
											The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">
												General
											</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10">
											<div class="form-group m-form__group row">
												<div class='col-md-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Stiri redactie
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">
												Activitate
											</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10">
											<div class="form-group m-form__group row">
												<div class='col-md-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Comentarii activitati proprii
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-md-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Inscrisi noi
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-md-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Raportari
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
												<div class='col-md-4'>
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-10 col-form-label">
															Mesaje
														</label>
														<div class="col-2">
															<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
																<label>
																	<input type="checkbox" checked="checked" name="">
																	<span></span>
																</label>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									
								</div>
							</form>
						
						
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function countyChanged(){
	alert('ai schimbat judetul, aducem localitati..');
}
</script>
@endsection