<header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
	<div class="m-container m-container--fluid m-container--full-height">
		<div class="m-stack m-stack--ver m-stack--desktop">
			<div class="m-stack__item m-brand  m-brand--skin-light ">
				<div class="m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-stack__item--middle m-brand__logo">
						<a href="index.html" class="m-brand__logo-wrapper">
							<img alt="" src="assets/demo/demo6/media/img/logo/logo_valuntariat_3.png"/>
						</a>
						<h3 class="m-header__title">
							Voluntar.eu
						</h3>
					</div>
					<div class="m-stack__item m-stack__item--middle m-brand__tools">
						<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>
						<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>
						<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
							<i class="flaticon-more"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
				<div class="m-header__title">
					<h3 class="m-header__title-text">
						VR
					</h3>
				</div>
				<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn">
					<i class="la la-close"></i>
				</button>
				<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light "  >
					<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
						<li class="m-menu__item  m-menu__item--active  m-menu__item--submenu m-menu__item--rel"  m-menu-submenu-toggle="click" aria-haspopup="true">
							<a  href="{{asset('/campanii')}}" class="m-menu__link m-menu__toggle">
								<span class="m-menu__item-here"></span>
								<span class="m-menu__link-text">
									Campanii
								</span>
								<i class="m-menu__hor-arrow la la-angle-down"></i>
							</a>
							<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
								<span class="m-menu__arrow m-menu__arrow--adjust"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/campanii')}}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon fa fa-tree"></i>
											<span class="m-menu__link-text">
												Ecologizare
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Plantare de arbori
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Curatare albii rauri
														</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/campanii')}}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-coins"></i>
											<span class="m-menu__link-text">
												Strangere de fonduri
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Cazuri sociale
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Proiecte Startup
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															ONG-uri
														</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/campanii')}}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon fa 	fa-hand-holding-heart"></i>
											<span class="m-menu__link-text">
												Sensibilizare
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Donare sange
														</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/campanii')}}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon fa 	fa-seedling"></i>
											<span class="m-menu__link-text">
												Divertisment
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
											<i class="m-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Concursuri
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Educatie
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/campanii')}}" class="m-menu__link ">
														<span class="m-menu__link-text">
															Sport
														</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel"  m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
							<a  href="javascript:;" class="m-menu__link m-menu__toggle">
								<span class="m-menu__item-here"></span>
								<span class="m-menu__link-text">
									Proiecte
								</span>
								<i class="m-menu__hor-arrow la la-angle-down"></i>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:600px">
								<span class="m-menu__arrow m-menu__arrow--adjust"></span>
								<div class="m-menu__subnav">
									<ul class="m-menu__content">
										<li class="m-menu__item">
											<h3 class="m-menu__heading m-menu__toggle">
												<span class="m-menu__link-text">
													Ecologizare
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</h3>
											<ul class="m-menu__inner">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/proiecte/1')}}" class="m-menu__link ">
														<i class="m-menu__link-icon fa 	fa-seedling"></i>
														<span class="m-menu__link-text">
															Ecologizare lac Bicaz
														</span>
													</a>
												</li>
											
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</li>
						<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
							<a  href="javascript:;" class="m-menu__link m-menu__toggle">
								<span class="m-menu__item-here"></span>
								<span class="m-menu__link-text">
									Oameni
								</span>
								<i class="m-menu__hor-arrow la la-angle-down"></i>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu  m-menu__submenu--fixed-xl m-menu__submenu--center" >
								<span class="m-menu__arrow m-menu__arrow--adjust"></span>
								<div class="m-menu__subnav">
									<ul class="m-menu__content">
										<li class="m-menu__item">
											<h3 class="m-menu__heading m-menu__toggle">
												<span class="m-menu__link-text">
													Organizatii
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</h3>
											<ul class="m-menu__inner">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Alexandru 
														</span>
													</a>
												</li>
												<hr />
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Toti
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link " >
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Inscrie-te!
														</span>
													</a>
												</li>
											</ul>
										</li>
										<li class="m-menu__item">
											<h3 class="m-menu__heading m-menu__toggle">
												<span class="m-menu__link-text">
													Manageri proiect
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</h3>
											<ul class="m-menu__inner">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Alexandru 
														</span>
													</a>
												</li>
												<hr />
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Toti
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Inscrie-te!
														</span>
													</a>
												</li>
											</ul>
										</li>
										<li class="m-menu__item">
											<h3 class="m-menu__heading m-menu__toggle">
												<span class="m-menu__link-text">
													Moderatori
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</h3>
											<ul class="m-menu__inner">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Alexandru 
														</span>
													</a>
												</li>
												<hr />
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Toti
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Inscrie-te!
														</span>
													</a>
												</li>
									
											</ul>
										</li>
										<li class="m-menu__item">
											<h3 class="m-menu__heading m-menu__toggle">
												<span class="m-menu__link-text">
													Administratori sistem
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</h3>
											<ul class="m-menu__inner">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Alexandru 
														</span>
													</a>
												</li>
												<hr />
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Toti
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="#" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-map"></i>
														<span class="m-menu__link-text">
															Inscrie-te!
														</span>
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</li>
						<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel m-menu__item--more m-menu__item--icon-only"  m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
							<a  href="javascript:;" class="m-menu__link m-menu__toggle">
								<span class="m-menu__item-here"></span>
								<span class="m-menu__link-text">
									Evenimente
								</span>
								<i class="m-menu__hor-arrow la la-angle-down"></i>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--pull">
								<span class="m-menu__arrow m-menu__arrow--adjust"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="inner.html" class="m-menu__link ">
											<i class="m-menu__link-icon flaticon-business"></i>
											<span class="m-menu__link-text">
												eCommerce
											</span>
										</a>
									</li>
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="crud/datatable_v1.html" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-computer"></i>
											<span class="m-menu__link-text">
												Audience
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
											<i class="m-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-users"></i>
														<span class="m-menu__link-text">
															Active Users
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-interface-1"></i>
														<span class="m-menu__link-text">
															User Explorer
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-lifebuoy"></i>
														<span class="m-menu__link-text">
															Users Flows
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-graphic-1"></i>
														<span class="m-menu__link-text">
															Market Segments
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-graphic"></i>
														<span class="m-menu__link-text">
															User Reports
														</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="inner.html" class="m-menu__link ">
											<i class="m-menu__link-icon flaticon-map"></i>
											<span class="m-menu__link-text">
												Marketing
											</span>
										</a>
									</li>
									<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="inner.html" class="m-menu__link ">
											<i class="m-menu__link-icon flaticon-graphic-2"></i>
											<span class="m-menu__link-title">
												<span class="m-menu__link-wrap">
													<span class="m-menu__link-text">
														Campaigns
													</span>
													<span class="m-menu__link-badge">
														<span class="m-badge m-badge--success">
															3
														</span>
													</span>
												</span>
											</span>
										</a>
									</li>
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="javascript:;" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-infinity"></i>
											<span class="m-menu__link-text">
												Cloud Manager
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
											<i class="m-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-add"></i>
														<span class="m-menu__link-title">
															<span class="m-menu__link-wrap">
																<span class="m-menu__link-text">
																	File Upload
																</span>
																<span class="m-menu__link-badge">
																	<span class="m-badge m-badge--danger">
																		3
																	</span>
																</span>
															</span>
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-signs-1"></i>
														<span class="m-menu__link-text">
															File Attributes
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-folder"></i>
														<span class="m-menu__link-text">
															Folders
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-cogwheel-2"></i>
														<span class="m-menu__link-text">
															System Settings
														</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel m-menu__item--more m-menu__item--icon-only"  m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
							<a  href="javascript:;" class="m-menu__link m-menu__toggle">
								<span class="m-menu__item-here"></span>
								<span class="m-menu__link-text">
									Logistica
								</span>
								<i class="m-menu__hor-arrow la la-angle-down"></i>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--pull">
								<span class="m-menu__arrow m-menu__arrow--adjust"></span>
								<ul class="m-menu__subnav">
									
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/contact')}}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-computer"></i>
											<span class="m-menu__link-text">
												Contact
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
											<i class="m-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/contact')}}" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-users"></i>
														<span class="m-menu__link-text">
															General
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/contact/parteneriate')}}" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-like"></i>
														<span class="m-menu__link-text">
															Parteneriate
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="{{asset('/contact/reclamatii')}}" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-alert-2"></i>
														<span class="m-menu__link-text">
															Reclamatii
														</span>
													</a>
												</li>
												
											</ul>
										</div>
									</li>
									<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/contact/2-la-suta')}}" class="m-menu__link ">
											<i class="m-menu__link-icon flaticon-coins"></i>
											<span class="m-menu__link-text">
												Doneaza 2%
											</span>
										</a>
									</li>
									<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/statistica/')}}" class="m-menu__link ">
											<i class="m-menu__link-icon flaticon-graphic-2"></i>
											<span class="m-menu__link-text">
												Statistica
											</span>
										</a>
									</li>
									<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="javascript:;" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-alert-2"></i>
											<span class="m-menu__link-text">
												Raportare
											</span>
											<i class="m-menu__hor-arrow la la-angle-right"></i>
											<i class="m-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
											<span class="m-menu__arrow "></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-signs-1"></i>
														<span class="m-menu__link-text">
															Campanie / Proiect
														</span>
													</a>
												</li>
												<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
													<a  href="inner.html" class="m-menu__link ">
														<i class="m-menu__link-icon flaticon-user"></i>
														<span class="m-menu__link-text">
															Membru
														</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
										<a  href="{{asset('/transparenta/')}}" class="m-menu__link ">
											<i class="m-menu__link-icon flaticon-search-magnifier-interface-symbol"></i>
											<span class="m-menu__link-text">
												Transparenta
											</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
				<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-light" id="m_quicksearch" m-quicksearch-mode="default">
						<form class="m-header-search__form">
							<div class="m-header-search__wrapper">
								<span class="m-header-search__icon-search" id="m_quicksearch_search">
									<i class="flaticon-search"></i>
								</span>
								<span class="m-header-search__input-wrapper">
									<input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="cautare.." id="m_quicksearch_input">
								</span>
								<span class="m-header-search__icon-close" id="m_quicksearch_close">
									<i class="la la-remove"></i>
								</span>
								<span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
									<i class="la la-remove"></i>
								</span>
							</div>
						</form>
						<div class="m-dropdown__wrapper">
							<div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
							<div class="m-dropdown__inner">
								<div class="m-dropdown__body">
									<div class="m-dropdown__scrollable m-scrollable" data-scrollable="true"  data-max-height="300" data-mobile-max-height="200">
										<div class="m-dropdown__content m-list-search m-list-search--skin-light"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="m-stack__item m-topbar__nav-wrapper">
						<ul class="m-topbar__nav m-nav m-nav--inline">
							<li class="m-nav__item m-topbar__notifications m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
								<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
									<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
									<span class="m-nav__link-icon">
										<span class="m-nav__link-icon-wrapper">
											<i class="flaticon-music-2"></i>
										</span>
									</span>
								</a>
								<div class="m-dropdown__wrapper">
									<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__header m--align-center">
											<span class="m-dropdown__header-title">
												9 New
											</span>
											<span class="m-dropdown__header-subtitle">
												User Notifications
											</span>
										</div>
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
															Alerts
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">
															Events
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">
															Logs
														</a>
													</li>
												</ul>
												<div class="tab-content">
													<div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
														<div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
															<div class="m-list-timeline m-list-timeline--skin-light">
																<div class="m-list-timeline__items">
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
																		<span class="m-list-timeline__text">
																			12 new users registered
																		</span>
																		<span class="m-list-timeline__time">
																			Just now
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge"></span>
																		<span class="m-list-timeline__text">
																			System shutdown
																			<span class="m-badge m-badge--success m-badge--wide">
																				pending
																			</span>
																		</span>
																		<span class="m-list-timeline__time">
																			14 mins
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge"></span>
																		<span class="m-list-timeline__text">
																			New invoice received
																		</span>
																		<span class="m-list-timeline__time">
																			20 mins
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge"></span>
																		<span class="m-list-timeline__text">
																			DB overloaded 80%
																			<span class="m-badge m-badge--info m-badge--wide">
																				settled
																			</span>
																		</span>
																		<span class="m-list-timeline__time">
																			1 hr
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge"></span>
																		<span class="m-list-timeline__text">
																			System error -
																			<a href="#" class="m-link">
																				Check
																			</a>
																		</span>
																		<span class="m-list-timeline__time">
																			2 hrs
																		</span>
																	</div>
																	<div class="m-list-timeline__item m-list-timeline__item--read">
																		<span class="m-list-timeline__badge"></span>
																		<span href="" class="m-list-timeline__text">
																			New order received
																			<span class="m-badge m-badge--danger m-badge--wide">
																				urgent
																			</span>
																		</span>
																		<span class="m-list-timeline__time">
																			7 hrs
																		</span>
																	</div>
																	<div class="m-list-timeline__item m-list-timeline__item--read">
																		<span class="m-list-timeline__badge"></span>
																		<span class="m-list-timeline__text">
																			Production server down
																		</span>
																		<span class="m-list-timeline__time">
																			3 hrs
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge"></span>
																		<span class="m-list-timeline__text">
																			Production server up
																		</span>
																		<span class="m-list-timeline__time">
																			5 hrs
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
														<div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
															<div class="m-list-timeline m-list-timeline--skin-light">
																<div class="m-list-timeline__items">
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
																		<a href="" class="m-list-timeline__text">
																			New order received
																		</a>
																		<span class="m-list-timeline__time">
																			Just now
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
																		<a href="" class="m-list-timeline__text">
																			New invoice received
																		</a>
																		<span class="m-list-timeline__time">
																			20 mins
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
																		<a href="" class="m-list-timeline__text">
																			Production server up
																		</a>
																		<span class="m-list-timeline__time">
																			5 hrs
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
																		<a href="" class="m-list-timeline__text">
																			New order received
																		</a>
																		<span class="m-list-timeline__time">
																			7 hrs
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
																		<a href="" class="m-list-timeline__text">
																			System shutdown
																		</a>
																		<span class="m-list-timeline__time">
																			11 mins
																		</span>
																	</div>
																	<div class="m-list-timeline__item">
																		<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
																		<a href="" class="m-list-timeline__text">
																			Production server down
																		</a>
																		<span class="m-list-timeline__time">
																			3 hrs
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
														<div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
															<div class="m-stack__item m-stack__item--center m-stack__item--middle">
																<span class="">
																	All caught up!
																	<br>
																	No new logs.
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
							<li class="m-nav__item" >
								<a href="{{asset('/cont')}}" class="m-nav__link m-dropdown__toggle">
									<span class="m-nav__link-icon ">
										<span class="m-nav__link-icon-wrapper">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</a>
							</li>
							<li class="m-nav__item m-topbar__user-profile" >
								<a href="#" class="m-nav__link m-dropdown__toggle" onclick="event.preventDefault();
										document.getElementById('logout-form').submit();">
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@csrf
									</form>
									<span class="m-nav__link-icon m-topbar__usericon">
										<span class="m-nav__link-icon-wrapper">
											<i class="flaticon-logout"></i>
										</span>
									</span>
								</a>
							</li>
							<li id="m_quick_sidebar_toggle" class="m-nav__item">
								<a href="#" class="m-nav__link m-dropdown__toggle">
									<span class="m-nav__link-icon m-nav__link-icon-alt">
										<span class="m-nav__link-icon-wrapper">
											<i class="flaticon-questions-circular-button"></i>
										</span>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>