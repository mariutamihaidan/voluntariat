
<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
			<i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">
			<!-- BEGIN: Aside Menu -->
<div 
id="m_ver_menu" 
class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " 
data-menu-vertical="true"
 m-menu-scrollable="1" m-menu-dropdown-timeout="500"  
>
				<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
					<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
						<a  href="javascript:;" class="m-menu__link m-menu__toggle">
							<i class="m-menu__link-icon flaticon-folder-1"></i>
							<span class="m-menu__link-text">
								Campaniile mele
							</span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
						</a>
						<div class="m-menu__submenu ">
							<span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								
								<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
									<a  href="inner.html" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Active
										</span>
									</a>
								</li>
								<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
									<a  href="inner.html" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Arhivate
										</span>
									</a>
								</li>
								
							</ul>
						</div>
					</li>
					
					<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
						<a  href="javascript:;" class="m-menu__link m-menu__toggle">
							<i class="m-menu__link-icon flaticon-layers"></i>
							<span class="m-menu__link-title">
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">
										Proiectele mele
									</span>
								
								</span>
							</span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
						</a>
						<div class="m-menu__submenu ">
							<span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"  m-menu-link-redirect="1">
									<span class="m-menu__link">
										<span class="m-menu__link-title">
											<span class="m-menu__link-wrap">
												<span class="m-menu__link-text">
													Active
												</span>
												
											</span>
										</span>
									</span>
								</li>
								<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
									<a  href="inner.html" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Arhivate
										</span>
									</a>
								</li>
								<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
									<a  href="javascript:;" class="m-menu__link m-menu__toggle">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Adauga proiect
										</span>
										<i class="m-menu__ver-arrow la la-angle-right"></i>
									</a>
									
								</li>
							</ul>
						</div>
					</li>
					
					
					<!--meniu voluntari-->
					
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
						<a  href="javascript:;" class="m-menu__link m-menu__toggle">
							<i class="m-menu__link-icon flaticon-users"></i>
							<span class="m-menu__link-title">
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">
										Voluntari
									</span>
								
								</span>
							</span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
						</a>
						<div class="m-menu__submenu ">
							<span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"  m-menu-link-redirect="1">
									<span class="m-menu__link">
										<span class="m-menu__link-title">
											<span class="m-menu__link-wrap">
												<span class="m-menu__link-text">
													Voluntari
												</span>
												
											</span>
										</span>
									</span>
								</li>
								<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
									<a  href="inner.html" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Toți voluntarii
										</span>
									</a>
								</li>
								<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
									<a  href="javascript:;" class="m-menu__link m-menu__toggle">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Adauga voluntar
										</span>
										<i class="m-menu__ver-arrow la la-angle-right"></i>
									</a>
									
								</li>
							</ul>
						</div>
					</li>
					
					
					
					<!-- sfarsit meniu voluntari -->
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				
					<li class="m-menu__section ">
						<h4 class="m-menu__section-text">
							Setari
						</h4>
						<i class="m-menu__section-icon flaticon-more-v3"></i>
					</li>
		
					<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
						<a  href="{{asset('/cont')}}" class="m-menu__link">
							<i class="m-menu__link-icon flaticon-profile"></i>
							<span class="m-menu__link-text">
								Editare cont
							</span>
						</a>
						
					</li>
				</ul>
			</div>
			<!-- END: Aside Menu -->
		</div>