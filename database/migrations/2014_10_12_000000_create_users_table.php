<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email',100)->unique();
			$table->boolean('is_admin')->default(false);
			$table->boolean('is_volunteer')->default(true);
			$table->integer('country_id')->default('40');
			$table->integer('county_id')->nullable();
			//$table->foreign('id_county')->references('id')->on('counties');
			$table->integer('city_id')->nullable();
			//$table->foreign('id_city')->references('id')->on('cities');
			
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
