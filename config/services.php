<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
	//// FACEBOOK NOT SET YET?//
    'facebook' => [
        'client_id' => '182433752589299',     
        'client_secret' => '76bc16e2c64417798d76c637c8e4b82b', 
        'redirect' => 'http://localhost/sal/public/login/facebook/callback',
    ],  
	'google' => [
        'client_id' => '438551145029-7jchgbbges6hv7pb6llccp16r6v70q8l.apps.googleusercontent.com',         
        'client_secret' => '_Mwy2nO5gxU3lgbv8bfIUUo_', 
        'redirect' => 'http://localhost/sal/public/login/google/callback',
    ],
	'linkedin' => [
        'client_id' => '86uog0y48omu9y',         
        'client_secret' => 'IfAqEl29FBQuUV08', 
        'redirect' => 'http://localhost/sal/public/login/linkedin/callback',
    ],
];
